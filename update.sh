#! /usr/bin/env bash
if [[ $1 == "catane" ]];
then
    ansible-playbook -i hosts -u root -f 10 -l $1 update-catane.yml || exit $?
elif [[ $1 != "" ]];
then
    ansible-playbook -i hosts -u root -f 10 -l $1 update-server.yml || exit $?
else
    echo -e "Usage: ./update <subset>\nExample: ./update slave"
fi
